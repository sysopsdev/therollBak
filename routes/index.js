var express = require('express');
var cool = require('cool-ascii-faces');
var router = express.Router();
var first = "The rollBak";
var second = "Latest Episode";
var third = "About";

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', {first: first, second: second, third: third});
});

router.get('/index', function(req, res) {
    res.redirect('/');
});

router.get('/episodes', function(req, res) {
    res.render('episodes', {first: first, second: second, third: third});
});

router.get('/cool', function(req, res) {
    res.send(cool());
});

router.get('/about', function(req, res) {
    res.render('about', {first: first, second: second, third: third});
});

router.get('/times', function(req, res) {
    var result = '';
    var times = process.env.TIMES || 5;
    for(var i = 0; i < times; i++) {
        result += i + ' ';
    }
    res.send(result);
});

module.exports = router;
